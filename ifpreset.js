Drupal.behaviors.ifpreset = function (context) {
  Drupal.ifpreset.check_dependency("#edit-data-horizontal-operation");
  Drupal.ifpreset.check_dependency("#edit-data-vertical-operation");
  Drupal.ifpreset.check_dependency("#edit-data-else-operation");

  $(".preset-operation").change(function() {
    Drupal.ifpreset.check_dependency("#" + this.id);
  });
}

Drupal.ifpreset = {

  check_dependency : function(item) {
    var operation = $(item).val();
    var preset = $(item).attr("name").slice(5, -12);

    $("#" + preset + "-preset").find(".preset-settings").parent().hide();
    $("#" + preset + "-preset").find("." + operation).parent().fadeIn();

    if ($("#edit-data-" + preset + "-upscale").hasClass(operation)) {
      $("#edit-data-" + preset + "-upscale").parent().parent().fadeIn();
    }
    else {
      $("#edit-data-" + preset + "-upscale").parent().parent().hide();
    }
  }
}
